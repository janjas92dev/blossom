<?php

declare(strict_types=1);

namespace App\Tests\Helper;

use ApiPlatform\Symfony\Bundle\Test\Client;
use App\Shared\Infrastructure\Doctrine\DataFixtures\ChildFixtures;
use App\Shared\Infrastructure\Doctrine\DataFixtures\RoomFixtures;
use Doctrine\ORM\EntityManagerInterface;

class CustomApiTestCase extends AbstractFixtureAwareTestCase
{

    public function setUp(): void
    {
        $this->addFixture(new RoomFixtures());
        $this->addFixture(new ChildFixtures());
        $this->executeFixtures(false);
        parent::setUp();
    }

    protected static function createClient(array $kernelOptions = [], array $defaultOptions = []): Client
    {
        return parent::createClient($kernelOptions, [
            'headers' => ['accept' => ['application/json']],
        ]);
    }

    protected function getEntityManager(): EntityManagerInterface
    {
        return self::getContainer()->get(EntityManagerInterface::class);
    }
}
