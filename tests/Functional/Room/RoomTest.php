<?php

declare(strict_types=1);

namespace App\Tests\Functional\Room;

use App\Shared\Infrastructure\Doctrine\Entity\Room;
use App\Tests\Helper\CustomApiTestCase;

/**
 * @group functional
 * @group room
 */
class RoomTest extends CustomApiTestCase
{

    public function testShouldCallGetItemEndpointThenCheckResponse()
    {
        // Given
        $client = self::createClient();
        $manager = $this->getEntityManager();
        $room = new Room();
        $room->setName('test');
        $room->setMaxCapacity(20);
        $manager->persist($room);
        $manager->flush();
        // When
        $response = $client->request('GET', sprintf('/rooms/%d', $room->getId()));
        //Then
        $this->assertResponseStatusCodeSame(200);
        $this->assertContains('application/json; charset=utf-8', $response->getHeaders()['content-type']);
        $arrayResponse = $response->toArray();
        $this->assertArrayHasKey('data', $arrayResponse);
        $this->assertArrayHasKey('id', $arrayResponse['data']);
        $this->assertArrayHasKey('name', $arrayResponse['data']);
        $this->assertArrayHasKey('maxCapacity', $arrayResponse['data']);
        $this->assertCount(3, $arrayResponse['data']);
    }

    public function testShouldCallGetStatisticsThenCheckResponse()
    {
        // Given
        $client = self::createClient();
        // When
        $response = $client->request('GET', '/rooms/statistics');
        //Then
        $this->assertResponseStatusCodeSame(200);
        $this->assertContains('application/json; charset=utf-8', $response->getHeaders()['content-type']);
        $arrayResponse = $response->toArray();
        $this->assertArrayHasKey('data', $arrayResponse);
        $this->assertSame(3, count($arrayResponse['data']));
        $this->assertArrayHasKey('room', $arrayResponse['data'][0]);
        $this->assertArrayHasKey('count', $arrayResponse['data'][0]);
        $this->assertSame(1, $arrayResponse['data'][0]['count']);
    }

    public function testShouldCallGetStatisticsWithFiltersThenCheckResponse()
    {
        // Given
        $client = self::createClient();
        // When
        $query = $this->getEntityManager()->getRepository(Room::class)->createQueryBuilder('r');
        $idList = $query->select('r.id')->setMaxResults(2)->getQuery()->getResult(6);
        $response = $client->request('GET', sprintf('/rooms/statistics?id[]=%d&id[]=%d', $idList[0], $idList[1]));
        // Then
        $this->assertResponseStatusCodeSame(200);
        $this->assertContains('application/json; charset=utf-8', $response->getHeaders()['content-type']);
        $arrayResponse = $response->toArray();
        $this->assertArrayHasKey('data', $arrayResponse);
        $this->assertSame(2, count($arrayResponse['data']));
    }

}
