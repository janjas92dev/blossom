<?php

declare(strict_types=1);

namespace App\Room\Domain\Repository;

interface RoomRepositoryInterface
{
    public function getById(int $id);

    public function getStatistics(array $findBy = []);
}
