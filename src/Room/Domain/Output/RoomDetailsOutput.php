<?php

declare(strict_types=1);

namespace App\Room\Domain\Output;

class RoomDetailsOutput
{
    public int $id;

    public string $name;

    public int $maxCapacity;

    /**
     * @param int $id
     * @param string $name
     * @param int $maxCapacity
     */
    public function __construct(int $id, string $name, int $maxCapacity)
    {
        $this->id = $id;
        $this->name = $name;
        $this->maxCapacity = $maxCapacity;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getMaxCapacity(): int
    {
        return $this->maxCapacity;
    }

}
