<?php

declare(strict_types=1);

namespace App\Room\Domain\Output;

class RoomStatisticsOutput
{
    public array $room;

    public int $count;

    /**
     * @param array $room
     * @param int $count
     */
    public function __construct(array $room, int $count)
    {
        $this->room = $room;
        $this->count = $count;
    }

    /**
     * @return array
     */
    public function getRoom(): array
    {
        return $this->room;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }


}
