<?php

declare(strict_types=1);

namespace App\Room\Infrastructure\ApiPlatform\Provider;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Room\Application\Query\GetRoomDetailsQuery;
use App\Room\Domain\Output\RoomDetailsOutput;
use App\Shared\Infrastructure\Doctrine\Entity\Room;
use App\Shared\Infrastructure\Symfony\Messenger\MessengerQueryBus;

class RoomItemProvider implements ProviderInterface
{


    public function __construct(private MessengerQueryBus $queryBus)
    {
    }

    public function provide(Operation $operation, array $uriVariables = [], array $context = [])
    {
        $room = $this->queryBus->dispatch(new GetRoomDetailsQuery($uriVariables['id']));
        if (!$room instanceof Room) {
            return [];
        }
        return new RoomDetailsOutput(
            $room->getId(),
            $room->getName(),
            $room->getMaxCapacity()
        );
    }
}
