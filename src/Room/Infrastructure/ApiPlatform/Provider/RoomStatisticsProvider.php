<?php

declare(strict_types=1);

namespace App\Room\Infrastructure\ApiPlatform\Provider;

use ApiPlatform\Metadata\Operation;
use ApiPlatform\State\ProviderInterface;
use App\Room\Application\Query\GetRoomStatisticsQuery;
use App\Room\Domain\Output\RoomStatisticsOutput;
use App\Shared\Infrastructure\Doctrine\Entity\Room;
use App\Shared\Infrastructure\Symfony\Messenger\MessengerQueryBus;

class RoomStatisticsProvider implements ProviderInterface
{

    public function __construct(private MessengerQueryBus $queryBus)
    {
    }

    public function provide(Operation $operation, array $uriVariables = [], array $context = [])
    {
        $result = $this->queryBus->dispatch(new GetRoomStatisticsQuery($context['filters'] ?? []));
        if ($result) {
            /** @var Room $room */
            foreach ($result as $room) {
                yield new RoomStatisticsOutput(['id' => $room->getId()], $room->getChilds()->count());
            }
        }
    }
}
