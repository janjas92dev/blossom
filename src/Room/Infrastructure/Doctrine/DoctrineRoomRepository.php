<?php

declare(strict_types=1);

namespace App\Room\Infrastructure\Doctrine;

use App\Room\Domain\Repository\RoomRepositoryInterface;
use App\Shared\Infrastructure\Doctrine\Repository\RoomRepository;

class DoctrineRoomRepository extends RoomRepository implements RoomRepositoryInterface
{

    public function getById(int $id)
    {
        return $this->findOneBy(['id' => $id]);
    }

    public function getStatistics(array $findBy = [])
    {
        if (count($findBy) > 0) {
            return $this->findBy($findBy);
        }
        return $this->findAll();
    }

}
