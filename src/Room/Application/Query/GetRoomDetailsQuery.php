<?php

declare(strict_types=1);

namespace App\Room\Application\Query;

use App\Shared\Application\Query\QueryInterface;

class GetRoomDetailsQuery implements QueryInterface
{
    public function __construct(private int $id)
    {
    }

    public function getId(): int
    {
        return $this->id;
    }
}
