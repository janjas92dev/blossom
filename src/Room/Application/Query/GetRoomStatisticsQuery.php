<?php

declare(strict_types=1);

namespace App\Room\Application\Query;

use App\Shared\Application\Query\QueryInterface;

class GetRoomStatisticsQuery implements QueryInterface
{

    public function __construct(private array $filterByIds = [])
    {
    }

    /**
     * @return array
     */
    public function getFilterByIds(): array
    {
        return $this->filterByIds;
    }

}
