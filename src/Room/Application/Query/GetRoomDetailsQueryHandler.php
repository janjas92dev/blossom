<?php

declare(strict_types=1);

namespace App\Room\Application\Query;

use App\Room\Domain\Repository\RoomRepositoryInterface;
use App\Shared\Application\Query\QueryHandlerInterface;

class GetRoomDetailsQueryHandler implements QueryHandlerInterface
{

    public function __construct(private RoomRepositoryInterface $repository)
    {
    }

    public function __invoke(GetRoomDetailsQuery $query)
    {
        return $this->repository->getById($query->getId());
    }
}
