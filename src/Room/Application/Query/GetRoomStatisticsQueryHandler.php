<?php

declare(strict_types=1);

namespace App\Room\Application\Query;

use App\Room\Domain\Repository\RoomRepositoryInterface;
use App\Shared\Application\Query\QueryHandlerInterface;

class GetRoomStatisticsQueryHandler implements QueryHandlerInterface
{


    public function __construct(private RoomRepositoryInterface $repository)
    {
    }

    public function __invoke(GetRoomStatisticsQuery $query)
    {
        return $this->repository->getStatistics($query->getFilterByIds());
    }
}
