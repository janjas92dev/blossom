<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Doctrine\Entity;

use ApiPlatform\Doctrine\Orm\Filter\SearchFilter;
use ApiPlatform\Metadata\ApiFilter;
use ApiPlatform\Metadata\ApiProperty;
use ApiPlatform\Metadata\ApiResource;
use ApiPlatform\Metadata\Get;
use ApiPlatform\Metadata\GetCollection;
use App\Room\Domain\Output\RoomDetailsOutput;
use App\Room\Domain\Output\RoomStatisticsOutput;
use App\Room\Infrastructure\ApiPlatform\Provider\RoomItemProvider;
use App\Room\Infrastructure\ApiPlatform\Provider\RoomStatisticsProvider;
use App\Shared\Infrastructure\Doctrine\Repository\RoomRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: RoomRepository::class)]
#[ApiResource(
    operations: [
        new GetCollection(
            '/rooms/statistics',
            openapiContext: ['summary' => 'Get list of room statistics'],
            paginationEnabled: false,
            output: RoomStatisticsOutput::class,
            provider: RoomStatisticsProvider::class
        ),
        new Get(
            '/rooms/{id}',
            openapiContext: ['summary' => 'Get Room details info'],
            paginationEnabled: false,
            output: RoomDetailsOutput::class,
            provider: RoomItemProvider::class,
        ),
    ]
)]
#[ApiFilter(SearchFilter::class, properties: ['id'])]
class Room
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255, nullable: false)]
    #[ApiProperty()]
    private ?string $name = null;

    #[ORM\Column(nullable: false)]
    private ?int $maxCapacity = null;

    #[ORM\OneToMany(mappedBy: 'room', targetEntity: Child::class)]
    private Collection $childs;

    public function __construct()
    {
        $this->childs = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getMaxCapacity(): ?int
    {
        return $this->maxCapacity;
    }

    public function setMaxCapacity(int $maxCapacity): self
    {
        $this->maxCapacity = $maxCapacity;

        return $this;
    }

    /**
     * @return Collection<int, Child>
     */
    public function getChilds(): Collection
    {
        return $this->childs;
    }

    public function addChilds(Child $childs): self
    {
        if (!$this->childs->contains($childs)) {
            $this->childs->add($childs);
            $childs->setRoom($this);
        }

        return $this;
    }

    public function removeChilds(Child $childs): self
    {
        if ($this->childs->removeElement($childs)) {
            // set the owning side to null (unless already changed)
            if ($childs->getRoom() === $this) {
                $childs->setRoom(null);
            }
        }

        return $this;
    }
}
