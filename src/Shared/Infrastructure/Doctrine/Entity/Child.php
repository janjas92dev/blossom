<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\Doctrine\Entity;

use ApiPlatform\Metadata\ApiResource;
use App\Shared\Infrastructure\Doctrine\Repository\ChildRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: ChildRepository::class)]
#[ApiResource(
    shortName: 'Child',
    operations: [],
    normalizationContext: [
        'groups' => ['child:read']
    ],
    denormalizationContext: [
        'groups' => ['child:write']
    ],
)]
class Child
{

    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id = null;

    #[ORM\Column(type: 'string', length: 255, nullable: false)]
    private ?string $name = null;

    #[ORM\Column(type: 'string', length: 255, nullable: false)]
    private ?string $surname = null;

    #[ORM\Column(type: 'boolean')]
    private bool $archived = false;

    #[ORM\ManyToOne(inversedBy: 'child')]
    private ?Room $room = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function setArchived(bool $archived): self
    {
        $this->archived = $archived;

        return $this;
    }

    public function isArchived(): bool
    {
        return $this->archived;
    }

    public function setRoom(?Room $room): self
    {
        $this->room = $room;

        return $this;
    }

    public function getRoom(): ?Room
    {
        return $this->room;
    }
}
