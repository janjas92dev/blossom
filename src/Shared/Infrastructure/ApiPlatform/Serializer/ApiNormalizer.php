<?php

declare(strict_types=1);

namespace App\Shared\Infrastructure\ApiPlatform\Serializer;

use Symfony\Component\Serializer\Normalizer\CacheableSupportsMethodInterface;
use Symfony\Component\Serializer\Normalizer\ContextAwareNormalizerInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareInterface;
use Symfony\Component\Serializer\Normalizer\NormalizerAwareTrait;

class ApiNormalizer implements ContextAwareNormalizerInterface, CacheableSupportsMethodInterface, NormalizerAwareInterface
{
    use NormalizerAwareTrait;
    private $supportOutput = false;

    private const ALREADY_CALLED = 'NORMALIZER_ALREADY_CALLED';

    public function hasCacheableSupportsMethod(): bool
    {
        return false;
    }

    public function supportsNormalization(mixed $data, string $format = null, array $context = []): bool
    {
        if (isset($context[self::ALREADY_CALLED])) {
            return false;
        }
        if (isset($context['output']) && isset($context['output']['class'])) {
            $this->supportOutput = true;
        } else {
            $this->supportOutput = false;
        }
        return true;
    }

    public function normalize(mixed $object, string $format = null, array $context = [])
    {
        $context[self::ALREADY_CALLED] = true;
        if ($this->supportOutput) {
            $result['data'] = $this->normalizer->normalize($object, $format, $context);
            return $result;
        }
        return $this->normalizer->normalize($object, $format, $context);
    }
}
